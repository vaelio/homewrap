from string import ascii_letters, digits
from os import urandom


def format(*args):
    return args[0].format(*args[1:])


def generateUsername(printable=ascii_letters+digits, maxlen=4):
    ret = []
    while len(ret) < maxlen:
        choice = ord(urandom(1))
        ret.append(printable[choice%len(printable)])
    return ''.join(ret)


