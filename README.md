# Homewrap

systemd-homed wrapper

## Introduction
### What the heck is systemd-homed
TL;DR: the future for our home directories on Linux. 

The point is to encrypt and sign your home directory by default and to get rid of the old passwd/shadow mecanism

Check the [man](https://www.freedesktop.org/software/systemd/man/systemd-homed.service.html) for more in-depth details

### What's the point of this tool then ?
The goal of the tool is to duplicate a base home with all our tools in another home before a pentest. 
This way each clients have their own data storage encrypted each with a different password.


### How secure is this ?
Newly created users and homes are encrypted using luks and signed. During my tests i could not leak any file whether or not  disks were mounted. The weakspot being that the password for the luks is the same than the one for the user meaning that all security sits on password strengh


## Prerequisites

### systemd-homed enabled and started
(should be there by default on any recent systemd distro)

```
systemctl enable --now systemd-homed
```

### check wether pam is configured to handle homed

Every distros have different pam setup. 
On archlinux it is already setup: if you grep in /etc/pam.d you should find:

```
[root@homed-poc ~]# grep -riH pam_systemd_home.so /etc/pam.d
/etc/pam.d/system-auth:-auth      [success=1 default=ignore]  pam_systemd_home.so
/etc/pam.d/system-auth:-account   [success=1 default=ignore]  pam_systemd_home.so
/etc/pam.d/system-auth:-password  [success=1 default=ignore]  pam_systemd_home.so
```

It's worth to try to use the same setup than arch but results may vary

## How to use

Simplest command is: ```sudo python wrapper.py deploy```

:warning: By default it will duplicate the home folder /home/pnt/ so make sure it exists or specify one that already exists
see an example for the full run :warning: :
```
[normy@homed-poc wrapper]$ sudo python wrapper.py deploy -m /home/normy/model/
Creating user pnt-i0k2 with model /home/normy/model/
homectl create pnt-i0k2 --umask=077 --disk-size=20Go --system=luks --skel=/home/normy/model/
🔐 Please enter new password for user pnt-i0k2: ****
🔐 Please enter new password for user pnt-i0k2 (repeat): (press TAB for no e****
It's all over, you can just run
sudo -u pnt-i0k2 -i bash
or login with username pnt-i0k2
[normy@homed-poc wrapper]$ sudo -u pnt-i0k2 -i bash
[sudo] password for %p:

pnt-i0k2 in homed-poc in ~
❯ id
uid=60018(pnt-i0k2) gid=60018(pnt-i0k2) groups=60018(pnt-qHq8)

```

:warning: Following the previous example, on fedora if you wanted to use toolbox (and podman) you would need to use the following command. This is because the userdata do not contain such informations yet

```
sudo usermod -v 100000:200000 -w 100000:200000 pnt-i0k2 
```

:warning: Values from previous example were chosen randomly

