from os import system
from sys import exit
from utils import *


class homectlClient(object):
    def __init__(self, model=None, username=None, dryrun=None, failonerror=True):

        self.model = model
        self.username = username
        self.dryrun = dryrun
        self.failonerror = failonerror

    def exec(self, cmd):
        print(cmd)
        if not self.dryrun:
            return system(cmd)
        return 0

    def deploy(self):

        print(
                format(
                "Creating user {} with model {}",
                    self.username,
                    self.model,
                    )
                )

        if self.exec(format("homectl create {} --disk-size=20G --skel={} --storage=luks --umask=077", self.username, self.model)) != 0:
            print("Failed to create new user, check if systemd-homed is enabled and running")
            if self.failonerror:
                exit(1)


        print("It's all over, you can just run")
        print(format("sudo -u {} bash -i", self.username))
        print(format("or login with username {}", self.username))

    
    def enter(self):

        print(
                format(
                    "Activating home for user {}",
                    self.username
                    )
                )

        
        if self.exec(format("homectl activate {}", self.username)) != 0:
            print("Could not activate home, check password ?")
            if self.failonerror:
                exit(2)


        print(
                format(
                    "Entering {}",
                    self.username
                    )
                )

        self.exec(format("sudo -u {} -i bash", self.username))

        print("All good !")
    


