#!/bin/env python3


from argparse import ArgumentParser
from os import urandom
from homectlClient import homectlClient
from utils import *


def createParser():
    parser = ArgumentParser()
    parser.add_argument("--dryrun", help="just print commands", action="store_true")
    parser.add_argument("-k", "--keep", help="do not fail on error", action="store_true")

    subparser = parser.add_subparsers(dest='subcmd', required=True)

    deploy = subparser.add_parser("deploy", help="deploy a new pnt user")
    deploy.add_argument(
            "-m", 
            "--model", 
            help="username of the model to dup (default: pnt)",
            default="pnt",
            required=False)
    deploy.add_argument(
            "-u", 
            "--user", 
            help="login of the new user(default: pnt-random)",
            type=str,
            metavar="username",
            nargs=1,
            required=False)

    enter = subparser.add_parser("enter", help="activate and enter new user")
    enter.add_argument(
            help="login of the new user(default: pnt-random)",
            type=str,
            metavar="username",
            dest="username",
            )
    
    return parser
    

def main():
    p = createParser()
    args = p.parse_args()

    dryrun = None if not args.dryrun else args.dryrun
    failonerror = True if not args.keep else False
    if args.subcmd == "deploy":
        model = "pnt" if not args.model else args.model
        username = format("pnt-{}", generateUsername()) if not args.user else args.user

        hc = homectlClient(
                model=model, 
                username=username, 
                dryrun=dryrun, 
                failonerror=failonerror
                )

        hc.deploy()

    if args.subcmd == "enter":
        username = args.username if args.username.startswith("pnt-") else format("pnt-{}", username)

        hc = homectlClient(
                model=None, 
                username=username, 
                dryrun=dryrun, 
                failonerror=failonerror
                )

        hc.enter()


if __name__ == '__main__':
    main()
